package company.matrix;

import company.bank.WithdrawMoney;

import java.util.Random;

/**
 * Created by heinr on 06.11.2016.
 */
public class Matrix {

    int[][] matrix_1;
    int[][] matrix_2;
    int[][] matrix_result;
    int height;

    Matrix(int height) {

        matrix_1 = new int[height][height];
        matrix_2 = new int[height][height];
        matrix_result = new int[height][height];

        if (height > 0) {

            Random rand = new Random();

            for (int i = 0; i < height; i++) {
                for (int j = 0; j < height; j++) {
                    this.matrix_1[i][j] = rand.nextInt(1939);
                    this.matrix_2[i][j] = rand.nextInt(1939);
                }
            }
        }
    }

    public static void main(String[] args) {

        Matrix matrix = new Matrix(4);
        Part part_1 = new Part(matrix.matrix_1, matrix.matrix_2, matrix.matrix_result, 4, 0, 4);
        part_1.start();
        try {
            part_1.join();
            for (int i = 0; i < part_1.height; i++) {
                for (int j = 0; j < part_1.height; j++) {
                    matrix.matrix_result[i][j] = part_1.matrix_result[i][j];
                    System.out.print(part_1.matrix_result[i][j] + " ");
                }
                System.out.println(" ");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(" ");

        int startLine_11 = 0;
        int finishLine_11 = 2;

        int startLine_21 = 2;
        int finishLine_21 = 4;

        Part part_11 = new Part(matrix.matrix_1, matrix.matrix_2, matrix.matrix_result, 4, startLine_11, finishLine_11);
        Part part_21 = new Part(matrix.matrix_1, matrix.matrix_2, matrix.matrix_result, 4, startLine_21, finishLine_21);
        part_11.start();
        part_21.start();
        try {
            part_11.join();
            part_21.join();
            for (int i = startLine_11; i < finishLine_11; i++) {
                for (int j = 0; j < part_11.height; j++) {
                    matrix.matrix_result[i][j] = part_11.matrix_result[i][j];
                    System.out.print(part_11.matrix_result[i][j] + " ");
                }
                System.out.println(" ");
            }
            for (int i = startLine_21; i < finishLine_21; i++) {
                for (int j = 0; j < part_21.height; j++) {
                    matrix.matrix_result[i][j] = part_21.matrix_result[i][j];
                    System.out.print(part_21.matrix_result[i][j] + " ");
                }
                System.out.println(" ");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(" ");

        int startLine_41 = 0;
        int finishLine_41 = 1;

        int startLine_42 = 1;
        int finishLine_42 = 2;

        int startLine_43 = 2;
        int finishLine_43 = 3;

        int startLine_44 = 3;
        int finishLine_44 = 4;

        Part part_41 = new Part(matrix.matrix_1, matrix.matrix_2, matrix.matrix_result, 4, startLine_41, finishLine_41);
        Part part_42 = new Part(matrix.matrix_1, matrix.matrix_2, matrix.matrix_result, 4, startLine_42, finishLine_42);
        Part part_43 = new Part(matrix.matrix_1, matrix.matrix_2, matrix.matrix_result, 4, startLine_43, finishLine_43);
        Part part_44 = new Part(matrix.matrix_1, matrix.matrix_2, matrix.matrix_result, 4, startLine_44, finishLine_44);
        part_41.start();
        part_42.start();
        part_43.start();
        part_44.start();
        try {
            part_41.join();
            part_42.join();
            part_43.join();
            part_44.join();
            for (int i = startLine_41; i < finishLine_41; i++) {
                for (int j = 0; j < part_41.height; j++) {
                    matrix.matrix_result[i][j] = part_41.matrix_result[i][j];
                    System.out.print(part_41.matrix_result[i][j] + " ");
                }
                System.out.println(" ");
            }
            for (int i = startLine_42; i < finishLine_42; i++) {
                for (int j = 0; j < part_42.height; j++) {
                    matrix.matrix_result[i][j] = part_42.matrix_result[i][j];
                    System.out.print(part_42.matrix_result[i][j] + " ");
                }
                System.out.println(" ");
            }
            for (int i = startLine_43; i < finishLine_43; i++) {
                for (int j = 0; j < part_11.height; j++) {
                    matrix.matrix_result[i][j] = part_43.matrix_result[i][j];
                    System.out.print(part_43.matrix_result[i][j] + " ");
                }
                System.out.println(" ");
            }
            for (int i = startLine_44; i < finishLine_44; i++) {
                for (int j = 0; j < part_21.height; j++) {
                    matrix.matrix_result[i][j] = part_44.matrix_result[i][j];
                    System.out.print(part_44.matrix_result[i][j] + " ");
                }
                System.out.println(" ");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}