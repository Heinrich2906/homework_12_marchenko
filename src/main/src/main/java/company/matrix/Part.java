package company.matrix;

import java.util.Random;

/**
 * Created by heinr on 06.11.2016.
 */
public class Part extends Thread {

    int[][] matrix_1;
    int[][] matrix_2;
    int[][] matrix_result;
    int height;
    int startLine;
    int finishLine;

    Part(int[][] matrix_1, int[][] matrix_2, int[][] matrix_result, int height, int startLine, int finishLine) {

        this.matrix_1 = new int[height][height];
        this.matrix_2 = new int[height][height];
        this.matrix_result = new int[height][height];

        this.height = height;
        this.startLine = startLine;
        this.finishLine = finishLine;

        if (height > 0) {

            for (int i = 0; i < height; i++) {
                for (int j = 0; j < height; j++) {
                    this.matrix_1[i][j] = matrix_1[i][j];
                    this.matrix_2[i][j] = matrix_2[i][j];
                }
            }
        }
    }

    @Override
    public void run() {
        if (startLine < 0 | finishLine < 0) {
            System.out.println("Start (finish) line is less then 0");
            return;
        }
        if (startLine > height | finishLine > height) {
            System.out.println("Start (finish) line is more then " + this.height);
            return;
        }

        if (startLine > finishLine) {
            System.out.println("Start line is more then Finish line");
            return;
        }

        for (int i = startLine; i < finishLine; i++) {

            for (int j = 0; j < this.height; j++) {

                matrix_result[i][j] = 0;

                for (int k = 0; k < this.height; k++) {
                    matrix_result[i][j] = matrix_result[i][j] + this.matrix_1[i][k] * this.matrix_2[k][j];
                }
            }
        }
    }
}
