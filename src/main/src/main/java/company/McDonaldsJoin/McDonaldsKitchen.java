package company.McDonaldsJoin;

import company.McDonalds.*;

/**
 * Created by heinr on 05.11.2016.
 */
public class McDonaldsKitchen {
    BigMacMenu bigMacMenu = new BigMacMenu();

    public static void main(String[] args) {

        company.McDonalds.McDonaldsKitchen mcDonaldsKitchen = new company.McDonalds.McDonaldsKitchen();
        mcDonaldsKitchen.createBigMacMenu();
    }

    public void createBigMacMenu() {

        Thread hamburger = new Thread(new Runnable() {
            @Override
            public void run() {

                System.out.println("Hamburger is been cocked...");


                try {
                    Thread.sleep(2000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                System.out.println("Hamburger is ready!");

            }
        });

        Thread fries = new Thread(new Runnable() {
            @Override
            public void run() {

                System.out.println("Fries is been cocked...");

                try {
                    Thread.sleep(2000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    hamburger.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                System.out.println("Fries is ready!");

            }


        });

        Thread cola = new Thread(new Runnable() {
            @Override
            public void run() {

                System.out.println("Cola is been cocked...");

                try {
                    Thread.sleep(2000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                try {
                    fries.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                System.out.println("Cola is ready!");
                System.out.println("Big Mac is ready!");

            }

        });

        cola.start();
        fries.start();
        hamburger.start();

        System.out.println("Ready! Steady! Go!");
    }
}
