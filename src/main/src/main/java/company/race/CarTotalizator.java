package company.race;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by heinr on 04.11.2016.
 */
public class CarTotalizator {
    public static void main(String[] args) throws IOException, InterruptedException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter a Lucky number(1...10): ");
        int luckyNumber = new Integer(reader.readLine());

        Car[] cars = new Car[11];

        for (int i = 1; i < 11; i++) {

            cars[i] = new Car(new Integer(i).toString());

            if (i == luckyNumber)
                cars[i].setPriority(Thread.MAX_PRIORITY);
            else cars[i].setPriority(Thread.MIN_PRIORITY);
        }

        for (int i = 1; i < 11; i++) {
            cars[i].start();
        }

    }
}
