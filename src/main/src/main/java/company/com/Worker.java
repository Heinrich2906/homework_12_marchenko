package company.com;

/**
 * Created by heinr on 31.10.2016.
 */
public class Worker extends Thread {

    Worker(String name) {
        setName(name);
    }

    @Override
    public void run() {

        int number = 1000;

        System.out.println("Worker " + this.getName() + " started to work!");

        for (int i = 1; i <= number; i++) {
            System.out.println("Detail number " + i + " is done by " + this.getName());
        }
    }
}
