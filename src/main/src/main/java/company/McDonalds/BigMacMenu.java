package company.McDonalds;

import java.util.Map;

/**
 * Created by heinr on 04.11.2016.
 */
public class BigMacMenu {

    private boolean isHamburgerReady;
    private boolean isFriesReady;
    private boolean isColaReady;
    private boolean isBigMacMenuReady;

    public boolean isHamburgerReady() {
        return isHamburgerReady;
    }

    public boolean isColaReady() {
        return isColaReady;
    }

    public boolean isBigMacMenuReady() {
        return isBigMacMenuReady;
    }

    public boolean isFriesReady() {
        return isFriesReady;
    }

    public void setHamburgerReady(boolean hamburgerReady) {
        isHamburgerReady = hamburgerReady;
    }

    public void setFriesReady(boolean friesReady) {
        isFriesReady = friesReady;
    }

    public void setColaReady(boolean colaReady) {
        isColaReady = colaReady;
    }

    public void setBigMacMenuReady(boolean bigMacMenuReady) {
        isBigMacMenuReady = bigMacMenuReady;
    }

    BigMacMenu(){

        this.isBigMacMenuReady = false;
        this.isHamburgerReady = false;
        this.isFriesReady = false;
        this.isColaReady = false;

    }
}
