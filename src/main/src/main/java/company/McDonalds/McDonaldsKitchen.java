package company.McDonalds;

/**
 * Created by heinr on 04.11.2016.
 */
public class McDonaldsKitchen {

    BigMacMenu bigMacMenu = new BigMacMenu();

    public static void main(String[] args) {

        McDonaldsKitchen mcDonaldsKitchen = new McDonaldsKitchen();
        mcDonaldsKitchen.createBigMacMenu();
    }

    public void createBigMacMenu() {

        Thread hamburger = new Thread(new Runnable() {
            @Override
            public void run() {

                System.out.println("Hamburger is been cocked...");

                synchronized (bigMacMenu) {
                    try {
                        Thread.sleep(2000L);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Hamburger is ready!");
                    bigMacMenu.setHamburgerReady(true);
                    bigMacMenu.notifyAll();
                }
            }
        });

        Thread fries = new Thread(new Runnable() {
            @Override
            public void run() {

                System.out.println("Fries is been cocked...");

                try {
                    Thread.sleep(2000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                synchronized (bigMacMenu) {

                    while (!bigMacMenu.isHamburgerReady()) {
                        try {
                            bigMacMenu.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                    System.out.println("Fries is ready!");
                    bigMacMenu.setFriesReady(true);
                    bigMacMenu.notifyAll();

                }

            }
        });

        Thread cola = new Thread(new Runnable() {
            @Override
            public void run() {

                System.out.println("Cola is been cocked...");

                try {
                    Thread.sleep(2000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                synchronized (bigMacMenu) {

                    while (!bigMacMenu.isFriesReady()) {
                        try {
                            bigMacMenu.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                    System.out.println("Cola is ready!");
                    System.out.println("Big Mac is ready!");
                    bigMacMenu.setColaReady(true);
                    bigMacMenu.notifyAll();
                }
            }
        });

        cola.start();
        fries.start();
        hamburger.start();

        System.out.println("Ready! Steady! Go!");
    }
}
