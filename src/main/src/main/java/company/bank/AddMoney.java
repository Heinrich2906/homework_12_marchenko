package company.bank;

import static company.bank.Bank.account;
import static company.bank.Bank.isBankomatWorking;

/**
 * Created by heinr on 06.11.2016.
 */
public class AddMoney extends Thread {

    Account account;

    AddMoney(Account account){
        this.account = account;
        start();
    }

    @Override
    public void run() {

        while (isBankomatWorking) {

            account.add(1000L);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
