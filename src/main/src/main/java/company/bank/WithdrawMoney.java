package company.bank;

import static company.bank.Bank.account;
import static company.bank.Bank.isBankomatWorking;

/**
 * Created by heinr on 06.11.2016.
 */
public class WithdrawMoney extends Thread {

    Account account;

    WithdrawMoney(Account account, String name) {

        this.account = account;
        this.setName(name);
        start();
    }

    @Override
    public void run() {

        while (isBankomatWorking) {

            System.out.print(getName() + " ");
            account.withdraw(500L);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
