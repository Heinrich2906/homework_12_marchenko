package company.bank;

/**
 * Created by heinr on 05.11.2016.
 */
public class Account {

    private Long balance;

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public void add(Long money) {
        this.balance = this.balance + money;
        System.out.println("Incom " + money + ". Balance " + balance);
    }

    public void withdraw(Long money) {
        this.balance = this.balance - money;
        System.out.println("Outcom " + money + ". Balance " + balance);
    }
}