package company.bank;

import static java.lang.Thread.sleep;

/**
 * Created by heinr on 05.11.2016.
 */
public class Bank {

    static Account account;
    public static volatile boolean isBankomatWorking;

    Bank(Long startBalance) {
        Account account = new Account();
        account.setBalance(startBalance);

    }

    public static void main(String[] args) {

        Bank bank = new Bank(1000L);

        Account account = new Account();
        account.setBalance(1000L);

        isBankomatWorking = true;

        AddMoney addMoney = new AddMoney(account);
        WithdrawMoney withdrawMoney_1 = new WithdrawMoney(account, "Ein");
        WithdrawMoney withdrawMoney_2 = new WithdrawMoney(account, "Zwei");
        WithdrawMoney withdrawMoney_3 = new WithdrawMoney(account, "Drei");

        try {
            sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        isBankomatWorking = false;
    }

}
